const tag = document.createElement('script');
const script = document.getElementsByTagName('script')[0];
const attr = 'data-youtube';
const movies = [];

tag.src = "//www.youtube.com/iframe_api";
script.parentNode.insertBefore(tag, script);

window.onYouTubeIframeAPIReady = () => {
  const elements = document.querySelectorAll(`[${attr}]`);
  movies.map.call(elements, (element ,index) => (
      new YT.Player(element, {
        height: '720',
        width: '405',
        videoId: element.getAttribute(attr),
        events: {
          onReady(event) {
            event.target.a.classList.add('is-ready');
          }
        },
        playerVars: {
          //'autoplay': index === 0 ? 1: 0,
          'controls': 0,
          'rel': 0,
          'fs': 1,
          'modestbranding': 1,
          'showinfo': 0
        }
      })
  ));
};
