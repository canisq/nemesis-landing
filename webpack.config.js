const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const WebpackPwaManifest = require('webpack-pwa-manifest');

const PUBLIC_PATH = 'public';

const config = {
  entry: {
    main: './src/main.js'
  },
  devtool: 'source-map',
  devServer: {
    contentBase: path.resolve(__dirname, PUBLIC_PATH)
  },
  module: {
    rules: [{
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.scss$/,
        use: ['style-loader', {
          loader: 'css-loader',
          options: {
            minimize: true
          }
        }, 'postcss-loader', 'sass-loader']
      }, {
        test: /\.(png|svg|jpg|gif)$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: '[name].[ext]?[hash]',
            outputPath: 'images/'
          }
        }, {
          loader: 'image-webpack-loader',
          options: {
            bypassOnDebug: true
          },
        }]
      },
      {
        test: /\.html$/,
        use: [{
          loader: 'html-loader',
          options: {
            minimize: true
          }
        }]
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin([PUBLIC_PATH]),
    new HtmlWebpackPlugin({
      favicon: 'src/favicon.ico',
      template: 'src/template.html',
      minify: true,
      filename: 'index.html',
      inject: true,
      hash: true
    }),
    new WebpackPwaManifest({
      name: 'Nemesis',
      short_name: 'Nemesis',
      description: 'Kickstarter Hit Board Game Nemesis',
      background_color: '#000000',
      theme_color: '#000000',
      'theme-color': '#000000',
      includeDirectory: true,
      fingerprints: false,
      icons: [
        {
          src: path.resolve('src/icons/icon-512x512.png'),
          sizes: [96, 128, 192, 256, 384, 512],
          destination: path.join('images','icons'),
          ios: {
            'apple-mobile-web-app-title': 'Nemesis',
            'apple-mobile-web-app-status-bar-style': 'black'
          }
        }
      ]
    })
  ],
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, PUBLIC_PATH),
    publicPath: '/'
  }
};

module.exports = config;
